function cmd_exists() {
    which $1 > /dev/null
}

if [ ! -d ~/.znap/znap ]; then
    mkdir -p ~/.znap
    git clone https://github.com/marlonrichert/zsh-snap ~/.znap/znap
fi

# not all systems support this :(
zstyle ':znap:*' git-maintenance off

source ~/.znap/znap/znap.zsh

# sane key bindings
zstyle ":prezto:module:editor" key-bindings emacs
zstyle ":prezto:module:editor" dot-expansion yes
export WORDCHARS='*?_-[]~&;!#$%^(){}<>'

# pretty colors
zstyle ":prezto:*:*" color yes
zstyle ":prezto:module:completion" color yes

# disable confirmation on rm and friends
zstyle ":prezto:module:utility" safe-ops no

zstyle ':completion:*' list-dirs-first true

zstyle ':prezto:module:terminal' auto-title 'always'

export PATH=~/.local/bin:~/.cargo/bin:~/.yarn/bin:~/.poetry/bin:$PATH:/opt/cisco/anyconnect/bin
export RIPGREP_CONFIG_PATH="$HOME/.config/ripgreprc"

function pmodload() {}

# helper always goes first, completion always goes last
znap source sorin-ionescu/prezto modules/{helper,environment,terminal,editor,history,directory,spectrum,utility,completion}
cmd_exists fzf && znap source reegnz/jq-zsh-plugin
znap source chisui/zsh-nix-shell
znap source https://gitlab.com/K900/poetry-zsh-plugin.git
znap source zsh-users/zsh-syntax-highlighting
cmd_exists fzf || znap source zsh-users/zsh-history-substring-search

if cmd_exists starship; then
    eval $(starship init zsh)
else
    znap source sindresorhus/pure async.zsh
    znap source sindresorhus/pure
fi

function up() {
    function _resolve_flake() {
        local flake_nix="$1/flake.nix"
        echo "${flake_nix:A:h}"
    }

    function _update_flake() {
        git -C "$1" pull origin master --rebase || return 1

        nix flake update "$1" --commit-lock-file
        git -C "$1" push
    }

    if cmd_exists nixos-rebuild; then
        local flake=$(_resolve_flake /etc/nixos)
        _update_flake "$flake"

        nixos-rebuild switch \
            --flake "$flake" \
            --impure \
            --use-remote-sudo \
            $@
    fi

    cmd_exists chezmoi && chezmoi update
    znap pull
}

function bump() {
    [ -f flake.lock ] && nix flake update && direnv reload
    [ -f Cargo.lock ] && cargo update
    [ -f pyproject.toml ] && poetry update --lock
    [ -f yarn.lock ] && yarn upgrade
    [ -f .pre-commit-config.yaml ] && pre-commit autoupdate
    [ -d .git ] && git ci -m "chore: bump"
}

alias ip='ip --color=auto'

if cmd_exists exa; then
    alias ls='exa --group-directories-first --classify --git --color-scale'
    alias l='exa -la --group-directories-first --classify --git --color-scale'
else
    alias ls='ls --color=always --group-directories-first --classify'
    alias l='ls -la --color=always --group-directories-first --classify'
fi

if cmd_exists bat; then
    export BAT_THEME=base16
    alias ccat='bat'
elif cmd_exists pygmentize; then
    alias ccat='pygmentize'
fi

if cmd_exists pikaur; then
    alias p='pikaur'
fi

if cmd_exists micro; then
    export EDITOR=micro
elif cmd_exists nvim; then
    export EDITOR=nvim
    alias vim=nvim
else
    export EDITOR=vim
fi

if cmd_exists pyenv; then
    eval "$(pyenv init -)"
fi

if cmd_exists broot; then
    broot --set-install-state refused
    eval "$(broot --print-shell-function zsh)"
fi

if cmd_exists fzf; then
    export FZF_DEFAULT_OPTS='--color=dark --color=fg:-1,bg:-1,hl:#c678dd,fg+:#ffffff,bg+:#4b5263,hl+:#d858fe --color=info:#98c379,prompt:#61afef,pointer:#be5046,marker:#e5c07b,spinner:#61afef,header:#61afef'
    local FZF_DIR="${$(which fzf):A:h:h}"
    source ${FZF_DIR}/share/fzf/completion.zsh
    source ${FZF_DIR}/share/fzf/key-bindings.zsh

    for keymap in "emacs" "viins"; do
        bindkey -M "$keymap" "$key_info[Up]" fzf-history-widget
    done
else
    for keymap in "emacs" "viins"; do
        bindkey -M "$keymap" "$key_info[Up]" history-substring-search-up
        bindkey -M "$keymap" "$key_info[Down]" history-substring-search-down
    done
fi

if cmd_exists keychain; then
    ssh_keys=$(cd ~/.ssh; \ls -1 | grep '^id_' | grep -v '[.]pub$')
    eval $(keychain --eval --quiet --agents ssh $ssh_keys)
fi

if cmd_exists rbw; then
    rbw unlock
    if ! ssh-add -l | grep -q stdin; then
        rbw get 'SSH key' | ssh-add -
    fi
fi

if cmd_exists direnv; then
    export DIRENV_LOG_FORMAT=""
    eval "$(direnv hook zsh)"
fi

if [ ! -z "${WSLPATH}" ]; then
    function win() {
        (
            export PATH="$PATH:$WSLPATH"
            exec $@
        )
    }

    alias code="win code"
fi

[ -f ~/.zshrc.site ] && source ~/.zshrc.site || true
